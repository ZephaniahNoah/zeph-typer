# Zeph Typer

A cross platform, educational typing game.

Dependencies:
* lwjgl-3.2.3
* lwjgl-glfw
* lwjgl-opengl
* lwjgl-stb
* pngdecoder
