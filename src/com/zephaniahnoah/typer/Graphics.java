package com.zephaniahnoah.typer;

import java.util.List;
import java.util.stream.Collectors;

import org.lwjgl.opengl.GL11;

public class Graphics {

	private static final Texture font;
	private static final int fontWidth = 21;// 21 // 10
	public static final int fontCharWidth = 24;// 24 //52
	public static final int fontCharHeight = 46;// 46 // 84
	private static final List<Character> fontChars = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ € ‚ƒ„…†‡ˆ‰Š‹Œ Ž  ‘’“”•–—˜™š›œ žŸ ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ".chars().mapToObj(c -> (char) c).collect(Collectors.toList());
	public static final int fontSpaceing = 30;
	public static final Color green = new Color(.1f, .8f, .2f);
	public static final Color darkGreen = new Color(.04f, .372f, .116f);
	public static final Color red = new Color(1, 0, 0);
	public static final Color white = new Color(1, 1, 1);
	private static int timer = 0;
	private static boolean done = false;

	static {
		font = new Texture("/font_grayscale_translucent.png");
	}

	// Wraps
	public static void drawText(String text, int x, int y, Color c, int maxLength, int spacer) {
		char[] list = text.toCharArray();
		int row = -1;
		for (int i = 0; i < list.length; i++) {
			if (i % maxLength == 0) {
				row++;
			}
			int index = fontChars.indexOf(list[i]);
			int xPos = index % fontWidth;
			int yPos = (int) Math.floor((float) index / (float) fontWidth);
			int yPosition = y - (row * fontCharHeight);
			drawTexRect(font, x + (i * fontSpaceing) - ((maxLength * fontSpaceing) * row), yPosition, fontCharWidth, fontCharHeight, xPos * fontCharWidth, yPos * fontCharHeight, fontCharWidth, fontCharHeight, c);
		}
	}

	public static void drawTypingText(String underText, String overText, int x, int y, int maxLength, int spacer) {
		char[] underList = underText.toCharArray();
		char[] overList = overText.toCharArray();
		int row = -1;
		for (int i = 0; i < underList.length; i++) {
			if (i % maxLength == 0) {
				row++;
			}
			char[] list = null;
			Color color = white;

			int fromX = x + (i * fontSpaceing) - ((maxLength * fontSpaceing) * row);
			int fromY = y - (row * fontCharHeight);
			if (overList.length > i) {
				list = overList;
				if (overList[i] == underList[i]) {
					color = green;
				} else if (overList[i] == ' ') {
					drawRect(fromX, fromY, fontCharWidth, fontCharHeight, red, 1);
				} else {
					color = red;
				}
			} else {
				list = underList;
				if (overList.length == i && Main.showCursor) {
					drawRect(fromX, fromY, fontCharWidth, fontCharHeight, darkGreen, 1);
				}
			}
			int index = fontChars.indexOf(list[i]);
			int xPos = index % fontWidth;
			int yPos = (int) Math.floor((float) index / (float) fontWidth);
			drawTexRect(font, x + (i * fontSpaceing) - ((maxLength * fontSpaceing) * row), fromY, fontCharWidth, fontCharHeight, xPos * fontCharWidth, yPos * fontCharHeight, fontCharWidth, fontCharHeight, color);
			if (overList.length > underList.length - 1) {
				done = true;
			}
			if (done) {
				timer++;
				if (timer == 1200) {
					Main.finishedLevel = true;
					Main.typeMode = false;
					timer = 0;
					done = false;
					break;
				}
			}
		}
	}

	public static void drawTexRect(Texture tex, int fromX, int fromY, int rectWidth, int rectHeight, int texX, int texY, int texWidth, int texHeight, Color c) {
		float windowWidth = Main.width;
		float windowHeight = Main.height;
		float x = (float) fromX / (windowWidth / 2) - 1f;
		float y = (float) fromY / (windowHeight / 2) - 1f;
		float width = (float) rectWidth / (windowWidth / 2);
		float height = (float) rectHeight / (windowHeight / 2);

		float startX = (float) texX / tex.width;
		float startY = (float) texY / tex.height;
		float endX = (float) texWidth / tex.width;
		float endY = (float) texHeight / tex.height;

		GL11.glColor4f(c.r, c.g, c.b, 1);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex.id);

		GL11.glBegin(GL11.GL_QUADS);

		GL11.glTexCoord2f(startX, startY + endY); // bottom left
		GL11.glVertex2f(x, y);
		GL11.glTexCoord2f(startX + endX, startY + endY); // bottom right
		GL11.glVertex2f(x + width, y);
		GL11.glTexCoord2f(startX + endX, startY); // top right
		GL11.glVertex2f(x + width, y + height);
		GL11.glTexCoord2f(startX, startY); // top left
		GL11.glVertex2f(x, y + height);

		GL11.glEnd();

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		GL11.glDisable(GL11.GL_TEXTURE_2D);

		GL11.glDisable(GL11.GL_BLEND);
	}

	public static void drawRect(int fromX, int fromY, int rectWidth, int rectHeight, Color c, float a) {
		float windowWidth = Main.width;
		float windowHeight = Main.height;
		float x = (float) fromX / (windowWidth / 2) - 1f;
		float y = (float) fromY / (windowHeight / 2) - 1f;
		float width = (float) rectWidth / (windowWidth / 2);
		float height = (float) rectHeight / (windowHeight / 2);

		GL11.glColor4f(c.r, c.g, c.b, a);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2f(x, y);
		GL11.glVertex2f(x + width, y);
		GL11.glVertex2f(x + width, y + height);
		GL11.glVertex2f(x, y + height);
		GL11.glEnd();

		GL11.glDisable(GL11.GL_BLEND);
	}
}
