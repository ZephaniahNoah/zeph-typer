package com.zephaniahnoah.typer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_REPEAT;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetCharCallback;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;
import java.util.Random;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;

public class Main {
	private static long window;
	private String name = "Zeph Typer";
	public static int width = 720;
	public static int height = 480;
	public static boolean finishedLevel = false;
	public static boolean typeMode = false;
	public static boolean showCursor = true;
	private static int selectionIndex = 0;
	private static double lastTime = 0;
	private static String renderText = "";
	private static String typingText = "";
	private static final String[] list = { "jkl;", "asdf", "hjkl;'", "asdfg", "ty", "gh", "qwert", "yuiop[]", "zxcvb", "nm,./", "abcdefghijklmnopqrstuvwxyz", "0123456789" };
	private static final String[] display = { list[0], list[1], list[2], list[3], list[4], list[5], list[6], list[7], list[8], list[9], "Alphabet", "Numbers" };
	private static Random rand = new Random();
	private static double startTime = 0;
	private static final int widthInChars = 22;
	private static final int heightInChars = 7;
	private static double cursorFlashTimer = 0;
	private static double kps = 0;
	private static int kpm = 0;

	public void run() {
		init();
		loop();
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void init() {
		System.out.println("VM: " + System.getProperty("java.version"));
		System.out.println("LWJGL: " + Version.getVersion());
		GLFWErrorCallback.createPrint(System.err).set();
		if (!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		window = glfwCreateWindow(width, height, name, NULL, NULL);
		if (window == NULL)
			throw new RuntimeException("Failed to create the GLFW window");

		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if (action == GLFW_PRESS || action == GLFW_REPEAT) {
				if (key == 259 && !(renderText.equals(""))) {
					renderText = renderText.substring(0, renderText.length() - 1);
					cursorFlashTimer = getTime();
				}

				if (!finishedLevel && !typeMode) {
					if (key == GLFW_KEY_DOWN) {
						selectionIndex++;
						if (selectionIndex == list.length) {
							selectionIndex = 0;
						}
					}

					if (key == GLFW_KEY_UP) {
						selectionIndex--;
						if (selectionIndex < 0) {
							selectionIndex = list.length - 1;
						}
					}

					if (key == GLFW_KEY_ENTER) {
						typeMode = true;
						finishedLevel = false;
						startTime = getTime();
						typingText = "";
						char[] letters = list[selectionIndex].toCharArray();
						for (int i = 0; i < widthInChars * heightInChars; i++) {
							int random = rand.nextInt(letters.length + 1);
							if (random == 0) {
								typingText += ' ';
							} else {
								typingText += letters[rand.nextInt(letters.length)];
							}
						}
					}
				}

				if (key == GLFW_KEY_ESCAPE)
					glfwSetWindowShouldClose(window, true);
			}
		});

		glfwSetCharCallback(window, (unknown, character) -> {
			if (typeMode) {
				renderText += (char) character;
				cursorFlashTimer = getTime();
			}
		});

		try (MemoryStack stack = stackPush()) {
			IntBuffer pWidth = stack.mallocInt(1);
			IntBuffer pHeight = stack.mallocInt(1);

			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			glfwGetWindowSize(window, pWidth, pHeight);
			glfwSetWindowPos(window, (vidmode.width() - pWidth.get(0)) / 2, (vidmode.height() - pHeight.get(0)) / 2);
		}

		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);

		glfwShowWindow(window);

		glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback() {
			@Override
			public void invoke(long id, int w, int h) {
				GL11.glViewport(0, 0, w, h);
				width = w;
				height = h;
			}
		});
	}

	private void loop() {
		GL.createCapabilities();
		while (!glfwWindowShouldClose(window)) {
			glClearColor(0.098f, 0.098f, 0.098f, 1.0f);
			double thisTime = getTime();

			if (cursorFlashTimer + 200 < thisTime) {
				if (!showCursor) {
					showCursor = true;
				}
				if (cursorFlashTimer + 950 < thisTime) {
					cursorFlashTimer = thisTime;
					showCursor = false;
				}
			}

			if (lastTime + 1000 / 30 < thisTime) {
				lastTime = thisTime;
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				int fontDistance = (Graphics.fontCharHeight + 10);
				int textX = 30;
				int textY = height - fontDistance;
				if (finishedLevel) {
					double lastKps = kps;
					double lastKpm = kpm;
					double finalTime = thisTime - startTime;
					double timeInSeconds = finalTime * .001;
					int totalCharSize = widthInChars * heightInChars;
					kps = totalCharSize / timeInSeconds;
					kpm = (int) Math.round(kps * 60);
					kps = Math.round(kps * 100) * 0.01D;
					// Average them
					if (lastKps != 0) {
						kpm = (int) Math.round((kpm + lastKpm) / 2d);
						kps = Math.round(((kps + lastKps) / 2D) * 100) * 0.01D;
					}
					finishedLevel = false;
					renderText = "";
				} else if (typeMode) {
					int maxWidth = (width - textX * 2) / Graphics.fontSpaceing;
					Graphics.drawTypingText(typingText, renderText, textX, textY, maxWidth, 10);
					Graphics.drawText("" + Math.round((thisTime - startTime) * .001), 6, 0, Graphics.white, textX, textY);
				} else {
					Graphics.drawRect(textX - 3, height / 2 - Graphics.fontCharHeight / 2, display[selectionIndex].length() * Graphics.fontSpaceing, Graphics.fontCharHeight, Graphics.darkGreen, 1f);
					for (int i = 0; i < display.length; i++) {
						Color color = Graphics.white;
						if (i == selectionIndex) {
							color = Graphics.green;
						}
						Graphics.drawText(display[i], textX, (height / 2 - Graphics.fontCharHeight / 2) + (selectionIndex - i) * fontDistance, color, width / Graphics.fontCharWidth + textX * 2, 10);
					}
					if (kps != 0) {
						String kpmText = "KPM: " + kpm;
						String kpsText = "KPS: " + kps;

						Graphics.drawText(kpmText, width - (kpmText.length() * Graphics.fontSpaceing), 0, Graphics.white, textX, textY);
						Graphics.drawText(kpsText, width - (kpsText.length() * Graphics.fontSpaceing), Graphics.fontCharHeight, Graphics.white, textX, textY);
					}
				}
				glfwSwapBuffers(window);
				glfwPollEvents();
			}
		}
	}

	public static double getTime() {
		return GLFW.glfwGetTime() * 1000;
	}

	public static void main(String[] args) {
		new Main().run();
	}
}
