package com.keyboard.typing;

/*
 * This is an example class.
 * Only used for practicing writing code.
 */
 
import java.util.ArrayList;
import java.util.List;

import net.finger.fingernail.IHangnail;
import org.atlas.Book;
import org.atlas.Position;

class Example extends Book implements IHangnail {

	private String name;
	private int height;
	private Position location;
	private byte size;
	private List<Byte> bytes;

	public Example(String name, int height, Position location, byte size) {
		this.name = name;
		this.height = height;
		this.location = location;
		this.size = size;
		bytes = new ArrayList<Byte>();
		for(int i = 0; i < 144; i++){
			bytes.add(i);
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getHeight() {
		return height;
	}

	public Position getLocation() {
		return location;
	}
	
	public byte getSize() {
		return size;
	}
	
	public int doMath(int number) {
		return (size * number) / bytes.get(5);
	}
}
